package de.friedenhagen.jacocokotlinsample

class App(val args: Array<String>) {
    fun doIt() {
        println("Hello World! ${args.toList()}")
    }
    companion object {
        fun main(args: Array<String>) {
            val app = App(args)
            app.doIt()
        }
    }
}

fun outer() {
    App.main(arrayOf())
}