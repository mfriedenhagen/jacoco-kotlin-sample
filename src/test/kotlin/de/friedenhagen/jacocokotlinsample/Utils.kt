package de.friedenhagen.jacocokotlinsample

import org.jacoco.core.analysis.Analyzer
import org.jacoco.core.analysis.CoverageBuilder
import org.jacoco.core.data.ExecutionData
import org.jacoco.core.data.ExecutionDataReader
import java.io.File
import java.io.FileInputStream

object Utils {

    fun executionDataVisitor(jacocoExecName: String): ExecutionDataVisitor {
        val visitor = ExecutionDataVisitor()
        FileInputStream(jacocoExecName).buffered().use {
            val reader = ExecutionDataReader(it)
            reader.setExecutionDataVisitor(visitor)
            reader.setSessionInfoVisitor(visitor)
            reader.read()
        }
        return visitor
    }

    fun coverageBuilder(klassName: String, visitor: ExecutionDataVisitor): CoverageBuilder {
        val coverageBuilder = CoverageBuilder()
        val analyzer = Analyzer(visitor.merged, coverageBuilder)
        val classFilesOfInterest = mutableListOf<String>(klassName + "Kt")
        classFilesOfInterest.addAll(
                visitor.merged.contents
                        .filter { classesOfInterest(it, klassName) }
                        .map { it.name })
        println("XXX Load coverage for ${classFilesOfInterest}")
        classFilesOfInterest.forEach { classFile ->
            val path = "${classFile}.class"
            val file = File("target/classes/${path}")
            if (file.exists())
                loadResource(file).use {
                    analyzer.analyzeClass(it, path)
                }
        }
        return coverageBuilder
    }

    fun loadResource(file: File) = FileInputStream(file).buffered()

    fun classesOfInterest(it: ExecutionData, klassName: String) = it.name.startsWith(klassName + "$") || it.name == klassName || it.name == klassName + "Kt"

    fun lines(klassName: String) = loadResource(File("src/main/kotlin/${klassName}.kt")).reader().readLines()

}