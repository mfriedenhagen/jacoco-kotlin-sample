package de.friedenhagen.jacocokotlinsample

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import org.junit.Test


class JaCoCoReportIT {

    private val jacocoExecName = "target/jacoco.exec"

    @Test
    fun testMyDataClass() {
        shouldLoad("de/friedenhagen/jacocokotlinsample/MyDataClass")
    }

    @Test
    fun testApp() {
        shouldLoad("de/friedenhagen/jacocokotlinsample/App")
    }
    private fun shouldLoad(klassName: String) {
        klassName.substringAfter("de/friedenhagen/jacocokotlinsample/")
        val visitor = Utils.executionDataVisitor(jacocoExecName)
        val coverageBuilder = Utils.coverageBuilder(klassName, visitor)
        val iClassCoverage = coverageBuilder.classes.first { it.name == klassName }
        val iSourceFileCoverage = coverageBuilder.sourceFiles.first { it.packageName + "/" + it.name == "${klassName}.kt" }
        assertThat(iClassCoverage.firstLine, equalTo(iSourceFileCoverage.firstLine))
        println("XXX ${klassName}: iClassCoverage.lastLine=${iClassCoverage.lastLine}, iSourceFileCoverage.lastLine=${iSourceFileCoverage.lastLine}")
        println("XXX ${klassName}: lines=${Utils.lines(klassName).size} lastLine=${iSourceFileCoverage.lastLine}")
    }

}