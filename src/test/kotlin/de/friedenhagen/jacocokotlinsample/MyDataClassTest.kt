package de.friedenhagen.jacocokotlinsample

import org.hamcrest.core.IsEqual.equalTo
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Test

class MyDataClassTest {
    @Test
    fun testIt() {
        val sut = MyDataClass("me", 18)
        val (name, age) = sut
        assertThat(name, equalTo("me"))
        assertThat(sut.name, equalTo("me"))
        assertThat(age, equalTo(18))
        assertThat(sut.age, equalTo(18))
        assertThat(sut, equalTo(sut.copy()))
        assertThat(sut.hashCode(), equalTo(sut.copy().hashCode()))
        assertNotNull(sut)
        assertThat(sut.toString(), equalTo("MyDataClass(name=me, age=18)"))
    }
}